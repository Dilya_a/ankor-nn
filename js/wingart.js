// $(function(){
// 	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
// 	ua = navigator.userAgent,

// 	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

// 	scaleFix = function () {
// 		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
// 			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
// 			document.addEventListener("gesturestart", gestureStart, false);
// 		}
// 	};
	
// 	scaleFix();
// });
// var ua=navigator.userAgent.toLocaleLowerCase(),
//  regV = /ipod|ipad|iphone/gi,
//  result = ua.match(regV),
//  userScale="";
// if(!result){
//  userScale=",user-scalable=0"
// }
// document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ



// INCLUDE FUNCTION
// ниже пример подключения табов. То есть создаем переменную с селектором на который нужно выполнить инициализацию, потом условием проверяем есть ли такой селектор, если есть то скрипт подключит файл с помощью функции INCLUDE. Это важно так как в наших проектах может быть подключено по 20 библиотек это оочеь плохо так как проект становится тяжелым и переполненным мусором

var sticky = $(".sticky"),
	matchheight = $("[data-mh]"),
	popup = $("[data-popup]"),
	owl = $(".owl-carousel"),
	slider = $(".flexslider"),
	rangeSlider = $("#slider-range"),
	styler = $(".styler"),
	fancyBox = $(".fancybox");

// if(tabsBox.length){
//   include("js/easyResponsiveTabs.js");
// }

	if($(fancyBox).length){
	  include("js/jquery.fancybox.js");
	}
	if($(owl).length){
	  include("js/owl.carousel.js");
	}
	if(matchheight.length){
		include("js/jquery.matchHeight-min.js");
	}
	if(popup.length){
		include('js/jquery.arcticmodal.js');
	}
	if(slider.length){
		include("js/jquery.flexslider.js");
	}
	if(sticky.length){
		include("js/sticky.js");
		include("js/jquery.smoothscroll.js");
	}
	if(rangeSlider.length){
		include('js/jquery-ui.js');
		include('js/jquery.ui.touch-punch.min.js');
	}
	if(styler.length){
		include("js/formstyler.js");
	}
// Плагин скриптом задает высоту блоку и выставляет по самому большому , нужно чтобы на блоке висел класс (data-mh) и атрибут (data-mh) с одним названием для одинаковых блоков, тогда он будет работать, выше находиться его инициализация, еще он хорошо работает при респонзиве. 

function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
}




$(document).ready(function(){

  $(".menu_link").on("click", function(){
  	$("body").addClass("navTrue");
  })
  $(".sub_item").on("mouseleave", function(){
		$("body").removeClass("navTrue");
	})

  	/* ------------------------------------------------
	FANCYBOX START
	------------------------------------------------ */

	if (fancyBox.length){
		fancyBox.fancybox({

		});
	}

	/* ------------------------------------------------
	FANCYBOX END
	------------------------------------------------ */

	/* ------------------------------------------------
	CAROUSEL START
	------------------------------------------------ */

	if(owl.length){
		owl.owlCarousel({
			singleItem : true,
			items : 1,
			smartSpeed:1000,
			nav: false,
			dot: true
		});
	}

	/* ------------------------------------------------
	CAROUSEL END
	------------------------------------------------ */

	/* ------------------------------------------------
	POPUP START
	------------------------------------------------ */

	if(popup.length){
		popup.on('click',function(){
		    var modal = $(this).data("popup");
		    $(modal).arcticmodal({
		    		beforeOpen: function(){
		    	}
		    });
		});
	};

	/* ------------------------------------------------
	POPUP END
	------------------------------------------------ */

	/* ------------------------------------------------
	STICKY START
	------------------------------------------------ */

		if (sticky.length){
			$(sticky).sticky({
		        topspacing: 0,
		        styler: 'is-sticky',
		        animduration: 0,
		        unlockwidth: false,
		        screenlimit: false,
		        sticktype: 'alonemenu'
			});
		};

	/* ------------------------------------------------
	STICKY END
	------------------------------------------------ */



	/* ------------------------------------------------
	FLEXSLIDER START
	------------------------------------------------ */

		if(slider.length){
			$('#main_carousel2').flexslider({
			    animation: "slide",
			    controlNav: false,
			    animationLoop: false,
			    slideshow: false,
			    itemWidth: 95,
			    itemMargin: 0,
			    asNavFor: '#main_slider2'
			});
			 
			$('#main_slider2').flexslider({
			    animation: "slide",
			    controlNav: false,
			    animationLoop: false,
			    smoothHeight: true,
			    slideshow: false,
			    sync: "#main_carousel2"
			});
		};
	/* ------------------------------------------------
	FLEXSLIDER END
	------------------------------------------------ */
	

	/* ------------------------------------------------
	RANGE-SLIDER START
	------------------------------------------------ */

		if(rangeSlider.length){
			rangeSlider.slider({
				touch: true,
				range: true,
				min: 0,
				max: 5000,
				values: [ 300, 4800 ],
				slide: function( event, ui ) {
					$( "#amount1" ).val( " " + ui.values[ 0 ] + " ");
					$( "#amount2" ).val( " " + ui.values[ 1 ] + " ");
				}
			});  
		    $( "#amount1" ).val( " " + $(rangeSlider).slider( "values", 0 ) + " " );
		    $( "#amount2" ).val( " " + $(rangeSlider).slider( "values", 1 ) + " " );

		    $( "#amount1" ).change(function(){
		    	var amount1 = $( "#amount1" ).val();
		    	var amount2 = $( "#amount2" ).val();

		    	if(parseInt(amount1) > parseInt(amount2)){
			        amount1 = amount2;
			        $( "#amount1" ).val(amount1);
			    }
			    rangeSlider.slider("values",0,amount1);
		    });

		    $( "#amount2" ).change(function(){
		    	var amount1 = $( "#amount1" ).val();
		    	var amount2 = $( "#amount2" ).val();

		    	if(parseInt(amount1) > parseInt(amount2)){
			        amount2 = amount1;
			        $( "#amount2" ).val(amount2);
			    }
			    rangeSlider.slider("values",1,amount2);
		    });

			rangeSlider.slider();  
	    }

	/* ------------------------------------------------
	RANGE-SLIDER END
	------------------------------------------------ */

	/* ------------------------------------------------
	FORMSTYLER START
	------------------------------------------------ */

		if (styler.length){
			styler.styler({
				
			});
		}

	/* ------------------------------------------------
	FORMSTYLER END
	------------------------------------------------ */

});